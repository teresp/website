# Clonación del Repositorio

Cómo clonar el repositorio, instalar dependencias y configuración de python.

## Requisitos

Tener instalado [Python 3](https://www.python.org/downloads/release/python-392/).

Tener instalado [Git](https://git-scm.com/).
Para usuarios de Windows siempre es recomendable utilizar **Git Bash** que viene incluída con Git (en lugar de CMD o PowerShell de Windows)




## Clonar

1. Crear una carpeta/directorio localmente, el nombre lo deciden ustedes. Se puede hacer con la consola/terminal con el comando:

```
mkdir Website-ourfirstitjob
```

2. Clonar el Repositorio de Gitlab 

```bash
git clone git@gitlab.com:ourfirstitjob/website.git
```

## Entorno Virtual (Python Virtual Enviroment VENV)

Una vez que clonamos el repositorio, procedemos a instalar las dependencias de Django.

3. Creamos el entorno virtual de Python con el comando `python -m venv [nombre_carpeta]`.

```
python -m venv venv
```

4. Ingresamos al entorno virtual. Esto permite que trabajemos en un entorno aislado y podamos instalar las dependencias necesarias con la versión correspondiente y que no tengan conflicto con otros proyectos que podamos tener en el equipo.

**Windows (con Git Bash)**

```
source venv/Scripts/activate 
```

**Windows (con PowerShell)**
```
 venv/Scripts/activate
```

**Mac/GNU-Linux**
```
 source venv/bin/activate
```

## Instalación de Dependencias de Django

El repositorio ya contiene la instalación del framework Django, pero necesitamos instalar las dependencias en el entorno virtual.contiene un listado de los paquetes que va a necesitar el proyecto)

5. Nos desplazamos al directorio del proyecto raíz (**website**)

```
cd ./website/
```

6. Instalamos los requerimientos con [pip](https://pypi.org/project/pip/).

**IMPORTANTE:** Antes asegurate que ingresaste al entorno virtual del paso 4.

```bash
pip install -r requeriments.txt

```

## Iniciar el server

7. Si todo se instaló correctamente, ya se puede montar el server y ver la página funcionando.

```bash
python manage.py runserver
```

Para ver que el server se está ejecutando accedé a http://localhost:8000